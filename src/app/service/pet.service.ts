import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Pet } from '../model/pet';
import { Observable } from 'rxjs';

@Injectable()
export class PetService {

  private petsUrl: string;

  constructor(private http: HttpClient) {
    this.petsUrl = 'http://localhost:8080/pets';
  }

  public findAll(): Observable<Pet[]> {
    return this.http.get<Pet[]>(this.petsUrl);
  }

  public deleteById(id: string) {
    console.log("deleteById");
    return this.http.delete<Pet>(this.petsUrl + "/" + id);
  }

  public findById(id: string) {
    console.log("findById");
    return this.http.get<Pet>(this.petsUrl + '/' + id);
  }

  public updatePet(pet: Pet) {
    return this.http.put<Pet>(this.petsUrl, pet).subscribe(data => console.log(data));
  }

  public save(pet: Pet){
    return this.http.post<Pet>(this.petsUrl, pet);
  }

}
