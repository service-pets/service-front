export class Pet {
    id: string;
    name: string;
    age: string;
    weight: string;
    color: string;
    type: string;
}
