import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PetListComponent } from './pet-list/pet-list.component';
import { PetFormComponent } from './pet-form/pet-form.component';
import { EditPetFormComponent } from './edit-pet-form/edit-pet-form.component';


const routes: Routes = [
  { path: 'pets', component: PetListComponent },
  { path: 'addpet', component: PetFormComponent},
  { path: 'editpet/:id', component: EditPetFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
