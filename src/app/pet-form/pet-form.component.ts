import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PetService } from '../service/pet.service';
import { Pet } from '../model/pet';

@Component({
  selector: 'app-pet-form',
  templateUrl: './pet-form.component.html',
  styleUrls: ['./pet-form.component.css']
})
export class PetFormComponent {

  pet: Pet;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private petService: PetService) {
    this.pet = new Pet();            
  }

  onSubmit() {
    this.petService.save(this.pet).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/pets']);
  }
    

}
