import { Component, OnInit } from '@angular/core';
import { Pet } from '../model/pet'
import { PetService } from '../service/pet.service';



@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {

  pets: Pet[];


  constructor(private petService: PetService) { }

  onDeletePet(pet: Pet){
    console.log(pet.id);
    this.petService.deleteById(pet.id).subscribe(result => this.ngOnInit());
  }

  ngOnInit() {
    this.petService.findAll().subscribe(data => {
      this.pets = data;
    });
  }
}

