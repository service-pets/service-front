import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPetFormComponent } from './edit-pet-form.component';

describe('EditPetFormComponent', () => {
  let component: EditPetFormComponent;
  let fixture: ComponentFixture<EditPetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
