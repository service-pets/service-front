import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PetService } from '../service/pet.service';
import { Pet } from '../model/pet';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-edit-pet-form',
  templateUrl: './edit-pet-form.component.html',
  styleUrls: ['./edit-pet-form.component.css']
})
export class EditPetFormComponent {

  pet: Pet;

  private id: string;
  private subscription: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private petService: PetService) {

      this.subscription = route.params.subscribe(params => this.id = params['id']);
      this.petService.findById(this.id).subscribe(data => {
        this.pet = data;
      });
  } 

  onSubmit() {
    this.petService.updatePet(this.pet);
      this.gotoUserList();
  }

  gotoUserList() {
    this.router.navigate(['/pets']);
  }


}
